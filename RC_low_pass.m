Fs = 1000;
t = linspace(0,1,Fs);
x = cos(2*pi*100*t)+0.5*randn(size(t));
fc = 150;
Wn = (2/Fs)*fc;
b = fir1(20,Wn,'low',kaiser(21,3));

fvtool(b,1,'Fs',Fs)
y = filter(b,1,x);

plot(t(1:100),x(1:100), 'g')
hold on
plot(t(1:100),y(1:100), 'r')

xlabel('Time (s)')
ylabel('Amplitude')
legend('Original Signal','Filtered Data')