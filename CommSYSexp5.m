t = 0:10^-6:10^-3;
amp_mess = 1.5;
amp_carrier = 0.2;
message = amp_mess*sin(2*pi*2000*t);
carrier = amp_carrier*cos(2*pi*10000*t);
message_phase = amp_mess*sin(2*pi*2000*t - pi);
carrier_phase = amp_carrier*cos(2*pi*10000*t - pi);
figure (1)
plot(t,message)
grid on
figure (2)
plot(t,carrier)
grid on
modulated_signal = message.*carrier;
figure (3)
plot(t,modulated_signal)
grid on
spectrum = (fft(modulated_signal));
figure (4)
plot(t,spectrum)
axis([-1*10^-3,2*10^-3,-5,5])
grid on