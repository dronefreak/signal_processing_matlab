t = 0:10^-6:8*10^-3;
amp_mess = 1.5;
amp_carrier = 1.2;
kp = 1;
Ac = 1;
message = amp_mess*square(2*pi*2000*t);
carrier = amp_carrier*sin(2*pi*10000*t);
s1 = 2*pi*10000*t + kp*message;
phase_modulated_signal = Ac*cos(s1);
figure (1)
plot(t,message)
grid on
figure (2)
plot(t,carrier)
grid on
figure (3)
plot(t,s1)
grid on
figure(4)
plot(t,phase_modulated_signal)
axis([0 0.3*10^-3 -2 1.5])
grid on
message_function = @(t)amp_mess*cos(2*pi*2000*t);
eval = integral(message_function,-Inf,0);
s2 = 2*pi*10000*t + eval;
freq_modulated_signal = Ac*cos(s2);
figure (5)
plot(t,s2)
grid on
figure(6)
plot(t,freq_modulated_signal)
grid on