t = 0:10^-6:10^-3;
amp_mess = 1.5;
amp_carrier = 0.2;
message = amp_mess*sin(2*pi*2000*t);
carrier = amp_carrier*cos(2*pi*10000*t);
message_phase = amp_mess*sin(2*pi*2000*t - pi);
carrier_phase = amp_carrier*cos(2*pi*10000*t - pi);
vdc = 2;
figure (1)
plot(t,message)
grid on
figure (2)
plot(t,carrier)
grid on
modulated_signal = (vdc + message).*carrier;
figure (3)
plot(t,modulated_signal)
grid on
z = abs(fft(modulated_signal));
figure (4)
plot(t,z)
grid on
modulated_signal_phase = (vdc + message_phase).*carrier_phase;
figure(5)
plot(t,modulated_signal_phase)
grid on
modulated_signal_rect = modulated_signal.*(modulated_signal>0);
modulated_signal_phase_rect = modulated_signal_phase.*(modulated_signal_phase>0);
figure(6)
plot(t,modulated_signal_rect)
grid on
figure(7)
plot(t,modulated_signal_phase_rect)
grid on
final_rect_signal = modulated_signal_rect + modulated_signal_phase_rect;
figure(8)
plot(t,final_rect_signal)
grid on

fsMHz = 10; % frequency, MHz
ip = final_rect_signal;
N = 4096; % Number of points for frequency analysis 

k = 0.05;
[h1F f1] = freqz([0 k],[1 -(1-k)],N,'whole',fsMHz);
op1 = filter([0 k],[1 -(1-k)],ip);

k = 0.2;
[h2F f2] = freqz([0 k],[1 -(1-k)],N,'whole',fsMHz);
op2 = filter([0 k],[1 -(1-k)],ip);

figure
plot([-N/2:N/2-1]*fsMHz/N,20*log10(abs(fftshift(h1F))),'m.-');
hold on
plot([-N/2:N/2-1]*fsMHz/N,20*log10(abs(fftshift(h2F))),'c.-');
axis([-fsMHz/2 fsMHz/2 -25 3])
grid on
xlabel('frequency, MHz')
ylabel('20log10(abs(H)))')
title('Magnitude response of the filter')
legend('k=0.1', 'k=0.2') 

figure
plot([0:length(ip)-1]/fsMHz,ip,'b.-');
hold on
plot([0:length(op1)-1]/fsMHz,real(op1),'m.-');
hold on
plot([0:length(op2)-1]/fsMHz,real(op2),'c.-');
xlabel('time, \mus')
ylabel('amplitude');
title('step response')
grid on
axis([0 22.4 0 5.2])
legend('ip', 'op1 k=0.1', 'op2 k=0.2')