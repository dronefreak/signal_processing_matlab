t = 0:10^-6:10^-3;
amp_mess = 4;
amp_carrier = 9;
message = amp_mess*sin(2*pi*2000*t);
message_phase = amp_mess*sin(2*pi*2000*t - (pi));
carrier = amp_carrier*cos(2*pi*10000*t);
rect_message = message.*(message>0);
figure (1)
plot(t,rect_message)
grid on
rect_message_phase = message_phase.*(message_phase>0);
figure (2)
plot(t,rect_message_phase)
grid on
rectified_signal = rect_message+rect_message_phase;
figure (3)
plot(t,rectified_signal)
grid on
Fs = 1000;
x = rectified_signal
fc = 150;
Wn = (2/Fs)*fc;
b = fir1(20,Wn,'low',kaiser(21,3));

fvtool(b,1,'Fs',Fs)
y = filter(b,1,x);

plot(t(1:100),x(1:100), 'g')
hold on
plot(t(1:100),y(1:100), 'r')

xlabel('Time (s)')
ylabel('Amplitude')
legend('Original Signal','Filtered Data')